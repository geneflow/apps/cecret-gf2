#!/bin/bash
#
# Title: cecret_runner.sh
# Description: A script to run the nextflow Cecret workflow
# Date Created: 2021-03-30 15:10
# Last Modified: Wed 31 Mar 2021 12:05:26 PM EDT
# Author: Reagan Kelly (ylb9@cdc.gov)
#

ml nextflow singularity

usage() { 
    echo "Usage: $0 -f <directory> -k <directory> -o <directory>"
}

while getopts "f:k:o:" opt; do
    case "${opt}" in
        f)
            sequencing_folder=${OPTARG}
            ;;
        k)
            kraken2_db=${OPTARG}
            ;;
        o)
            outdir=${OPTARG}
            ;;
        h)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

if [[ -z $sequencing_folder || -z $kraken2_db || -z $outdir ]]; then
    echo "Sequencing folder, kraken2_db folder, and output folder are all required fields"
else
    nextflow run Cecret.nf -c config/singularity.config --kraken2 true --kraken2_db "$kraken2_db" --reads "$sequencing_folder" --outdir "$outdir"
fi
